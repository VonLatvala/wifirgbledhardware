# WiFiRGBLEDHardware

24V RGB driver supporting up to ~1.5A current per channel, and is controlled by WiFi.

EasyEDA schematic, PCB layout, BOM, GERBER files and datasheets. Open straight in the EasyEDA web editor by clicking [here](https://easyeda.com/vonlatvala/wifi-rgb-led-controller-rev2).

For firmware, please visit [WiFiWSLED](https://bitbucket.org/VonLatvala/wifiwsled/src/master/)

For frontend software please visit [LairOZ for iPhone](https://bitbucket.org/VonLatvala/lairoz-ios/src/master/) or [LairOZ Web](https://bitbucket.org/VonLatvala/lairoz-web/src/master/)

**WARNING**: Please do use genuine WeMos modules, as counterfeit ones have been spotted with underrated voltage regulators, causing them to reset when using full power. If I'm not mistaken, their `official` store is located under [AliExpress](https://lolin.aliexpress.com/store/1331105).

Also do note that I'm using revision 2.X of the development board, while it seems to be discontinued. I have not checked if the footprints match, so keep this in mind if you are ordering a rev 3.0+ of the module.

**WARNING**: When designing modules that use `ESP8266`, please note that specific pins have to be in specific configurations during boot. In case you connect pins without careful consideration, you might by mistake change bootmode to [ROM serial bootloader for esptool](https://github.com/espressif/esptool/wiki/ESP8266-Boot-Mode-Selection#select-bootloader-mode), which won't boot up your program. Pins that need to be in a specific configuration on boot: D4 (GPIO2) HIGH WITH RESISTOR, D8 (GPIO15) LOW RESISTOR OPTIONAL. These things are true for atleast WeMos D1 Mini Pro, although the actual GPIO names will be correct for any ESP8266 module. The DX and AX pins are just what WeMos call them in this specific module.

# Images

## Technical up-to-date documents (SVG)

![Schematic](images/rev/2_0/schematic_main.svg "Schematic")
![PCB Top](images/rev/2_0/PCB_top.svg "PCB Top")
![PCB Bottom](images/rev/2_0/PCB_bottom.svg "PCB Bottom")
![PCB Full](images/rev/2_0/PCB_full.svg "PCB Full")

## IRL up-to-date images

![Top view in Hammock box with WeMos module](images/rev/2_0/top_view_with_wemos.jpg "Top view in Hammock box with WeMos module")
![Top view in Hammock box without WeMos module](images/rev/2_0/top_view_without_wemos.jpg "Top view in Hammock box without WeMos module")
![Top view outside box without WeMos module](images/rev/2_0/top_view_outside_box.jpg "Top view outside box without WeMos module")
![Bottom view](images/rev/2_0/bot_view_with_components.jpg "Bottom view")
![Naked PCB](images/rev/2_0/PCB_top_bot.jpg "Naked PCB")

## Older prototype images

These use 220Ω resistors, and the transistors here are [ON Semiconductor](https://www.onsemi.com/) [BD675 NPN Darlington BJTs](https://www.onsemi.com/pub/Collateral/BD675-D.PDF). They also were designed to be run at 12V, but this turned out to be worse than 24V to drive longer strips, the colors faded in the end of the strip when using 12V, that's why the REV2 design is rated at 24V.

![Top View](images/rev/1_0/open_top_view.jpg "Top View")
![Side View 1](images/rev/1_0/open_side_view_1.jpg "Side View 1")
![Side View 2](images/rev/1_0/open_side_view_2.jpg "Side View 2")
![Side View 3](images/rev/1_0/open_side_view_3.jpg "Side View 3")
![Side View 4](images/rev/1_0/open_side_view_4.jpg "Side View 4")
![Closed Side View 1](images/rev/1_0/closed_side_view_1.jpg "Closed Side View 1")
![Closed Side View 2](images/rev/1_0/closed_side_view_2.jpg "Closed Side View 2")
![Closed Side View 3](images/rev/1_0/closed_side_view_3.jpg "Closed Side View 3")
![Closed Side View 4](images/rev/1_0/closed_side_view_4.jpg "Closed Side View 4")

